begin
    
    # Reads the name of the file in this case test.tx located on the same folder
    puts "What is the file name?"
    file = gets.chomp
   
    # Reads the number N and convert it to integer
    puts "Whats is the N number?"
    myN = Integer(gets.chomp)

rescue ArgumentError

    #If the read value is not an integer print error and retry
    puts "That's not a integer, try again"

    retry
  
    
end

# Will open file and for each line will get the max value defined in myN and then sort it
topN = File.open(file).each_line.max(myN){|a,b| a.to_i <=> b.to_i}
# Will print the Top Values 
puts topN